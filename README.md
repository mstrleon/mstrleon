<h1 align="center">Hello World, I'm Leon 👋</h1>

<img align="right" alt="GIF" src="https://mstrleon.gitlab.io/code.gif" width="300" />
<h3 align="center">I’m a Web Developer <br> based in Thailand</h3>

<br/>

- 🤝 I’m looking to take part in projects, creating web and mobile apps, which are helpful to people.
- 🔭 I’m currently working on: 1) Phangan.yoga - a yoga schools/teachers aggregator. 2) EmoMeter - emotion intelligence training app 3) do Product management work at Psyphi Soul Line - working on ChronoMethod web app
- 🌱 I’m currently mastering backend programming
- 👨 As a part of Tapmod.Studio dev team I took part in several projects below, including their website itself: [look here](https://tapmod.studio/#portfolio)
- 📫 My email sunman.le@gmail.com
- 🔍 My <a href="https://www.linkedin.com/in/leon-anufriev-30a73a1a6/" target="blank">LinkedIn</a>
- 📄 Know about my experiences: [download resume](https://mstrleon.gitlab.io/resumeLA2.pdf)
- 🌞 Fun fact: I've been developing Java2ME games for monochrome mobile phones back in 2004!
- 🚂  I have been living in Thailand for more than 11 years. I am originally from Russia and Ukraine
- 🎉 My other life-long passions are Music, Yoga, Freediving, DIY with wood and electronics


<h2>Some of my works:</h2>
<ul>
<li> ⚡️ Home Cinema 3d Configurator    👉 <a href="https://hts-coach.tapmod.studio/" target="blank">live</a> | <a href="https://gitlab.com/mstrleon/hts-coach" target="blank">src</a>
</li>
<li> ⚡️ Emerald Guitar 3d Confugurator (together with Tapmod.Studio dev team)    👉 <a href="https://emeraldguitars.com/builder/#/x30" target="blank">live</a> 
</li>
<!--<li> ⚡️ Crypto Crims Avatar Generator   👉 <a href="https://crypto-crims.netlify.app/" target="blank">demo</a> | <a href="https://gitlab.com/mstrleon/crypto-crims" target="blank">src</a>
</li>
<li> ⚡️ Avatar NFT Minter    👉 <a href="https://ava-minter.herokuapp.com/" target="blank">demo</a> | <a href="https://gitlab.com/mstrleon/avatar-minter" target="blank">src</a>
</li>-->
<li> ⚡️ Digital Blockchain Passport     👉 <a href="https://digital-passport.netlify.app/" target="blank">demo</a> | <a href="https://gitlab.com/mstrleon/did-ethereum-vue" target="blank">src</a>
</li>
<!--<li> ⚡️ Mobile IoT climate health statistics dashboard Demo   👉 <a href="https://pcs-stats.netlify.app/" target="blank">demo</a> | <a href="https://gitlab.com/mstrleon/pcs-mobile" target="blank">src</a>
</li>-->
<li> ⚡️ Samsung Phones 3d Presentation    👉 <a href="https://q3roadshow.tapmod.studio/" target="blank">live</a> 
</li>
<li> ⚡️ Dashboard D3 Demo for ETF Investors (Russian language)    👉 <a href="https://dash-ii.netlify.app/" target="blank">demo</a> 
</li>
</ul>


<h3 align="left">Tech Stack:</h3>
<p align="left"> 

<!--<a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> --> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Typescript_logo_2020.svg/1024px-Typescript_logo_2020.svg.png" alt="typescript" width="40" height="40"/> </a> <a href="https://sass-lang.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/sass/sass-original.svg" alt="sass" width="40" height="40"/> </a> <a href="https://stylus-lang.com" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Stylus-logo.svg/393px-Stylus-logo.svg.png?20151221102449" alt="sass" width="40" height="40"/> </a> <a href="https://vuejs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="40" height="40"/> </a> <a href="https://quasar.dev/" target="_blank" rel="noreferrer"> <img src="https://cdn.quasar.dev/logo/svg/quasar-logo.svg" alt="quasar" width="40" height="40"/> </a> <a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> <a href="https://d3js.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/d3js/d3js-original.svg" alt="d3js" width="40" height="40"/> </a> <a href="https://jestjs.io" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/jestjsio/jestjsio-icon.svg" alt="jest" width="40" height="40"/> </a> <a href="https://vitest.dev/" target="_blank" rel="noreferrer"> <img src="https://vitest.dev/logo-shadow.svg" alt="jest" width="40" height="40"/> </a>
<!--<a href="https://mochajs.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/mochajs/mochajs-icon.svg" alt="mocha" width="40" height="40"/> </a> -->

<a href="https://nodejs.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-original-wordmark.svg" alt="nodejs" width="40" height="40"/> </a> <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a> 

<a href="https://soliditylang.org/" target="_blank" rel="noreferrer"> <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Solidity_logo.svg/1319px-Solidity_logo.svg.png" alt="Solidity" width="40" height="40"/> </a> <a href="https://ceramic.network" target="_blank" rel="noreferrer"> <img src="https://assets.website-files.com/609ab8eae6dd417c085cc925/609b2ba76d637745d781160e_logo-ceramic.png" alt="Solidity" width="40" height="40"/> </a> 

<a href="https://sketchfab.com/" target="_blank" rel="noreferrer"> <img src="https://static.sketchfab.com/static/builds/web/dist/static/assets/images/favicon/a81e1fd93fc053fed8a5f56640f886f8-v2.png" alt="Sketchfab" width="40" height="40"/> </a> 

</p>



